//Problem : User when clicking on image goes to a dead end
//Solution: Create an overlay with the large image - Ligthbox

var $overlay = $('<div id="overlay"></div>');
var $image = $('<img id="image">');
var $caption = $("<p></p>");
var $title = $("<p></p>");
var $leftArrow = $('<img id="previous" src="photos/left_arrow.png">');
var $rightArrow = $('<img id="next" src="photos/right_arrow.png">');

// GT - Declare current_index/count_photos variables
var current_index = 0;
var count_photos = $("#imageGallery li").length;

//An image to overlay
$overlay.append($image);

//A caption to overlay
$overlay.append($caption);

//A title to overlay
$overlay.append($title);

////Arrows to overlay
$overlay.append($leftArrow);
$overlay.append($rightArrow);

//Add overlay
$("body").append($overlay);

//Capture the click event on a link to an image
$("#imageGallery a").click(function (event) {
    event.preventDefault();
    var imageLocation = $(this).attr("href");

    //Update overlay with the image linked in the link
    $image.attr("src", imageLocation); // GT - Replace attr href->src

    /* GT - Set current index */
    current_index = $(this).closest("li").index();

    //Show the overlay
    $overlay.show();

    //Get child's alt attribute and set caption
    var captionText = $(this).children("img").attr("alt");
    $caption.text(captionText);

    var titleText = $(this).children("img").attr("title");
    $title.text(titleText);

});
  

//When overlay is clicked 
$overlay.click(function(){
  //Hide the overlay
  $overlay.hide();
}).children().click(function(e) { // GT - Click function exclude children (#previous, #next).
  return false;
});

//Capture the click event on the arrows
$("#previous").click(function () {
    current_index--; // GT - Increment current index
    if(current_index < 0){
        current_index = count_photos-1; // GT - Set last element
    }    
    var imageLocation = $("#imageGallery li").eq(current_index).find("a").attr("href"); // GT - Set image location
    $image.attr("src", imageLocation); // GT - Replace img src
});

$("#next").click(function () {
    current_index++; // GT - Decrase current index
    if(current_index == count_photos){
        current_index = 0; // GT - Set first element
    }
    var imageLocation = $("#imageGallery li").eq(current_index).find("a").attr("href"); // GT - Set image location
    $image.attr("src", imageLocation); // GT - Replace img src
});